﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocomotionSimpleAgent : MonoBehaviour {

	public Transform lookAtTarget;
	private UnityEngine.AI.NavMeshAgent agent;
	private Animator anim;
	private Vector2 smoothDeltaPosition = Vector2.zero;
	private Vector2 velocity = Vector2.zero;

	// Use this for initialization
	void Start () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		anim = GetComponent<Animator>();

		// positionの更新をしない
		agent.updatePosition = false;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

		// worldDeltaPositionをローカル空間にMap
		// 標準化
		float dx = Vector3.Dot(transform.right, worldDeltaPosition);
		float dy = Vector3.Dot(transform.forward, worldDeltaPosition);
		Vector2 deltaPosition = new Vector3(dx, dy);

		// deltaMoveにローパスフィルターを適用
		float smooth = Mathf.Min(1.0f, Time.deltaTime/0.15f);
		smoothDeltaPosition = Vector2.Lerp(smoothDeltaPosition, deltaPosition, smooth);

		// 速度更新
		if(Time.deltaTime > 1e-5f)
			velocity = smoothDeltaPosition / Time.deltaTime;

		// agent.remainingDistance: agent-tagetの距離
		bool shoudMove = velocity.magnitude > 0.5f && agent.remainingDistance > agent.radius;

		// アニメーション更新
		anim.SetBool("run", shoudMove);
		anim.SetFloat("velx", velocity.x);
		anim.SetFloat("vely", velocity.y);

		// LookAt
		LookAt lookAt = GetComponent<LookAt>();
		if(lookAt){
			// lookAt.lookAtTargetPosition = agent.steeringTarget + transform.forward;
			lookAt.lookAtTargetPosition = lookAtTarget.position;
		}

		// if (worldDeltaPosition.magnitude > agent.radius)
        //             transform.position = agent.nextPosition - 0.9f*worldDeltaPosition;
	}

	void OnAnimatorMove(){
		// positionをagentの位置に更新
		transform.position = agent.nextPosition;
	}
}

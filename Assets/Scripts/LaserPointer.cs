﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {

	[SerializeField]
	private Transform _RightHandAnchor;

	[SerializeField]
	private Transform _LeftHandAnchor;

	[SerializeField]
	private Transform _CenterEyeAnchor;

	[SerializeField]
	private float _MaxDistance = 100.0f;

	[SerializeField]
	private LineRenderer _LaserPointerRenderer;

	public bool ShowLaserPointer = true;

	private Transform Pointer {
		get {
			var controller = OVRInput.GetActiveController();
			if(controller == OVRInput.Controller.RTrackedRemote){
				return _RightHandAnchor;
			}else if(controller == OVRInput.Controller.LTrackedRemote){
				return _LeftHandAnchor;
			}
			return _CenterEyeAnchor;

		}
	}
	// Update is called once per frame
	void Update () {
		var pointer = Pointer;
		if (pointer == null || _LaserPointerRenderer == null){
			return;
		}

		Ray pointerRay = new Ray(pointer.position, pointer.forward);

		if(ShowLaserPointer)
			_LaserPointerRenderer.SetPosition(0, pointerRay.origin);

		RaycastHit hitInfo;
		if(Physics.Raycast(pointerRay,  out hitInfo, _MaxDistance)){
	
			if(ShowLaserPointer)
				_LaserPointerRenderer.SetPosition(1, hitInfo.point);

			// do the hitting object
			// hitしたオブジェクトを取得
			GameObject obj = hitInfo.collider.gameObject;
			// hitしたオブジェクトのScaleを取得
			Vector3 scale = obj.transform.localScale;
			if(OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger)){
				// TriggerButtonを押したとき
			}else if(OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad)){
				// touchpad buttonを押したとき
			}

		}else {
			// 向いている方向に伸ばす
			if(ShowLaserPointer)
				_LaserPointerRenderer.SetPosition(1, pointerRay.origin + pointerRay.direction * _MaxDistance);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMobileGyro : MonoBehaviour {
	public bool isUnityRemote;
	private Quaternion gyro;

	// Use this for initialization
	void Start () {
		if(isUnityRemote)
			Input.gyro.enabled = true;	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.gyro.enabled){
			gyro = Input.gyro.attitude;
			transform.localRotation = Quaternion.Euler(90, 0, 0) * (new Quaternion(-gyro.x, -gyro.y, gyro.z, gyro.w));
		}
	}
}

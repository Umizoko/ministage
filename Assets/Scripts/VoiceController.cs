﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceController : MonoBehaviour {

	public Transform target;
	public AudioClip[] voices;
	public float interval = 4.0f;

	private AudioSource audio;
	private UnityEngine.AI.NavMeshAgent agent;
	private Animator anim;

	private bool shoudShow;
	private float lastInterval;

	void Start() {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		anim = GetComponent<Animator>();
		audio = GetComponent<AudioSource>();
		shoudShow = false;
		lastInterval = Time.realtimeSinceStartup;
		StartCoroutine(VoiceRoutine());
	}

	// Use this for initialization
	IEnumerator VoiceRoutine () {
		while(Application.isPlaying){
			// targetに到達、RUNの音声を停止
			yield return new WaitUntil(() => shoudShow);
			audio.Stop();
			yield return new WaitForSeconds(1.30f);
			// SHOWの音声を再生
			// audio.clip = voices[1];
			audio.PlayOneShot(voices[1], 0.7F);
			yield return new WaitForSeconds(3f);
			// JUMPの音声を再生
			// audio.clip = voices[2];
			audio.PlayOneShot(voices[2], 0.7F);
			yield return new WaitForSeconds(1f);
			// RUNの音声を再生
			audio.clip = voices[0];
			audio.Play();
			shoudShow = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(transform.position, target.position);
		if(distance < 0.5f && (Time.realtimeSinceStartup - lastInterval) > interval){
			shoudShow = true;
			lastInterval = Time.realtimeSinceStartup;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

	public Transform[] points;
	private int destPoint = 0;
	private UnityEngine.AI.NavMeshAgent agent;

	// Use this for initialization
	void Start () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

		// 目標地点を断続的に移動
		agent.autoBraking = false;

		GotoNextPoint();	
	}
	
	void GotoNextPoint(){
		if(points.Length == 0){
			return;
		}

		agent.destination = points[destPoint].position;

		// 次の目標値に移動
		destPoint = (destPoint + 1) % points.Length;
	}

	// Update is called once per frame
	void Update () {
		if(!agent.pathPending && agent.remainingDistance < 0.5f){
			GotoNextPoint();
		}	
	}
}

# miniStage

UnityちゃんがStage上を走りまわるVRアプリ

![image](Screenshot/miniStage.jpg)

![image](Screenshot/miniStage.gif)


## Description

VRアプリ

公式のVR Samplesを参照しながら制作した。

シーン遷移、入力、UI、モデル、アニメーション、サウンドなど

一通りの要素を組み込んだ。

## Requirement
- Unity2018
- unity chan © UTJ/UCL

## 対応ハード（確認済み）

Oculus Go

Android
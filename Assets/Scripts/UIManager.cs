﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {
	[SerializeField] private Reticle m_Reticle;
	[SerializeField] private SelectionRadial m_Radial;
	[SerializeField] private UIFader m_UIFader;
	[SerializeField] private SelectionSlider m_MainStartSlider;
	[SerializeField] private SelectionSlider m_ExitSlider;
	[SerializeField] private string sceneName;

    [SerializeField] private VRCameraFade m_VRCameraFade;           // Reference to the script that fades the scene to black.


	void Start(){
		StartCoroutine(MainStart());
		StartCoroutine(Exit());
	}

	IEnumerator Exit(){
		m_Reticle.Show();
		m_Radial.Hide();
		yield return StartCoroutine (m_ExitSlider.WaitForBarToFill());
		yield return StartCoroutine (m_UIFader.InteruptAndFadeOut ());
        yield return StartCoroutine (m_VRCameraFade.BeginFadeOut(true));
		Debug.Log("Exit");
		Application.Quit();
	}

	IEnumerator MainStart(){
		m_Reticle.Show();
		m_Radial.Hide();
		yield return StartCoroutine (m_MainStartSlider.WaitForBarToFill ());
		yield return StartCoroutine (m_UIFader.InteruptAndFadeOut ());
        yield return StartCoroutine (m_VRCameraFade.BeginFadeOut (true));
		Debug.Log("MainStart");
		if(sceneName != null){
			SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
		}
	}
}

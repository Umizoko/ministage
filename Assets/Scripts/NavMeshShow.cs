﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMeshShow : MonoBehaviour {
	public Transform target;
	public float interval = 4.0f;
	private UnityEngine.AI.NavMeshAgent agent;
	private Animator anim;

	private bool shoudShow;
	private float lastInterval;

	// Use this for initialization
	void Start () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		anim = GetComponent<Animator>();
		shoudShow = false;
		lastInterval = Time.realtimeSinceStartup;
		StartCoroutine(NavMeshRoutine());
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(transform.position, target.position);
		if(distance < 0.5f && (Time.realtimeSinceStartup - lastInterval) > interval){
			shoudShow = true;
		}
	}

	IEnumerator NavMeshRoutine(){
		while(Application.isPlaying){
			yield return new WaitUntil(() => shoudShow);
			agent.isStopped = true;
			anim.SetTrigger("show");
			yield return new WaitForSeconds(4f);
			agent.isStopped = false;
			shoudShow = false;
			lastInterval = Time.realtimeSinceStartup;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageRotation : MonoBehaviour {

	private float rotationY;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		rotationY += Time.deltaTime * 10;
		transform.rotation = Quaternion.Euler(0, rotationY, 0);
	}
}

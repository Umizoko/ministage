﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowToGUIController : MonoBehaviour {

	// [SerializeField]
	private CanvasGroup canvasGroup;
	private bool displayenabled;
	private Animator anim;
	
	void Start() {
		canvasGroup = GetComponent<CanvasGroup>();
		displayenabled = false;
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		if(OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad)){
			displayenabled = !displayenabled;
		}

		if(displayenabled){
			anim.SetBool("Start", displayenabled);
		}else{
			anim.SetBool("Start", displayenabled);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NavMeshJump : MonoBehaviour {

	public Transform target;
	private UnityEngine.AI.NavMeshAgent agent;
	private Animator anim;

	// Use this for initialization
	void Start () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		anim = GetComponent<Animator>();
		StartCoroutine(NavMeshRoutine());
	}
	
	IEnumerator NavMeshRoutine(){
		agent.autoTraverseOffMeshLink = false;
		while(Application.isPlaying){
			yield return new WaitUntil(() => agent.isOnOffMeshLink);
			agent.isStopped = true;
			agent.updatePosition = true;
			anim.SetTrigger("jump");
			yield return new WaitForSeconds(1.0f);
			yield return transform.DOLocalJump(agent.currentOffMeshLinkData.endPos, 4, 1, 1.0f).WaitForCompletion();
			yield return new WaitForSeconds(0.8f);
			agent.CompleteOffMeshLink();
			agent.isStopped = false;
			agent.updatePosition = false;
		}
	}
}

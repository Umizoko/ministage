﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPlaneMove : MonoBehaviour {

	public Transform target;

	private Rigidbody rb;
	private float noizeY;
	private float distance;
	private Vector3 prePos;
	private float angle;
	private float offsetAngle;
	private float noiseCount;
	private float noiseCountAdd;
	private float noiseYAdd;


	private float maxAccel = 0.01f;
	public float MaxHight = 8f;



	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		noizeY = Random.Range(0f, 1f);
		distance = Vector2.Distance(new Vector2(target.position.x, target.position.z), new Vector2(transform.position.x, transform.position.z));
		offsetAngle = Vector2.Angle(Vector2.right, new Vector2(transform.position.x, transform.position.z));
		prePos = Vector3.zero;
		noiseCount = Random.Range(0f, 1f);
		noiseCountAdd = Random.Range(0.001f, 0.05f);
		noiseYAdd = Random.Range(0f, 0.005f);
	}
	
	void Update () {
		Vector3 nextPosXZ = distance * new Vector3(Mathf.Cos(offsetAngle + angle), 0, -Mathf.Sin(offsetAngle + angle));
		Vector3 nextPosY = Vector3.up * Mathf.PerlinNoise(0, noizeY) * 5 + Vector3.up*MaxHight;
		Vector3 nextPos = nextPosXZ + nextPosY;

		// Rotate
		transform.LookAt(nextPos - prePos);
		// Debug.Log(nextPos - prePos);

		// Move
		transform.position = nextPos;

		prePos = nextPos;
		noizeY += noiseYAdd;
		angle += Mathf.PerlinNoise(0, noiseCount) * maxAccel;
		noiseCount = noiseCountAdd;
	}
}
